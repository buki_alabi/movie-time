# Movie Time
This application queries the [open movie database](https://www.omdbapi.com/) for movie descriptions and displays the results in a user-friendly way

## Development server
If you are setting up this for the first time you might need to run the below commands:
`git checkout dev && git fetch`
`npm install`

Run `npm start` for a dev server. Navigate to [http://localhost:8080/](http://localhost:8080/). If port 8080 is in use, another port will be automatically selected. See the logs in the terminal to know which port has been selected.

## Testing
This project uses browser based testing. Follow the instructions above to run the development server and navigate to the "test.html" file in the "test" folder. For instance, if the project is running on port 8080, open [http://localhost:8080/test/test.html](http://localhost:8080/test/test.html) in your browser.

## Distribution files
Run `npm run build` to build the es5 compatible javascript file in the "dist" folder.

Upload the "dist" folder, the "assets" folder, the "css" folder and the "index.html" file to your server of choice.
