export class ModalService {
	constructor() {
		this.body = document.getElementsByTagName("body")[0];
		this.loader = document.getElementById("loading");
	}

	initializeAllModals() {
		this.intializeImageModals();
		this.initializeModalClosers();
		this.initializeSearchTrigger();
	}

	openModal(modalId) {
		let modal = document.getElementById(modalId);
		modal.style.display = "block";
		this.body.style.overflow = "hidden";
		return modal;
	}

	closeModal(modalId) {
		document.getElementById(modalId).style.display = "none";
		this.body.style.overflow = "auto";
	}

	intializeImageModals() {
		let images = document.querySelectorAll("img.hover-card");
		for (let image of images) {
			image.addEventListener("click", () => {
				let modal = this.openModal("img-modal");
				modal.querySelector("img").src = image.src;
			});
		}
	}

	initializeModalClosers() {
		let modal_closers = document.querySelectorAll(".modal-close");
		for (let closer of modal_closers) {
			closer.addEventListener("click", event => {
				this.closeModal(event.target.parentElement.id);
			});
		}
	}

	initializeSearchTrigger() {
		document.getElementById("search-btn").addEventListener("click", () => {
			this.openModal("search");
			document.getElementById("search-input").focus();
		});
	}

	initializeCastCards() {
		let castCards = document.querySelectorAll("#cast .card");
		for (let card of castCards) {
			card.addEventListener("click", () => {
				this.loader.style.display = "block";
				setTimeout(() => {
					this.openModal("cast-modal");
					this.loader.style.display = "none";
				}, 500);
			});
		}
	}
}
