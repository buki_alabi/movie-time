export class TabService {
	constructor() {
		this.tabLine = document.getElementById("tab-line");
		this.tabSections = document.getElementsByClassName("tab-section");
		this.selectedTab = null;
	}

	initializeTabs() {
		this.initializeTabHeadings();
		this.initializeStickyTabs();
		window.addEventListener("resize", () => {
			if (this.selectedTab) this.showTab(this.selectedTab);
		});
	}

	/**
	 * Adds click listeners to all tab headings and automatically activates the first tab
	 * All tab headings are anchor tags with id's that end with "-heading"
	 * Splitting the id lets you know what tab to show e.g "cast-heading" will show the "cast" tab
	 */
	initializeTabHeadings() {
		let tabHeadings = document.querySelectorAll("a[id$='-heading']");
		for (let heading of tabHeadings) {
			heading.addEventListener("click", event => {
				let tabSectionID = event.target.id.split("-")[0];
				this.showTab(tabSectionID);
			});
		}
		if (tabHeadings[0]) {
			tabHeadings[0].click();
		}
	}

	initializeStickyTabs() {
		let tabHeadings = document.getElementById("tab-headings");
		let tabTopSpace = tabHeadings.offsetTop;
		window.onscroll = function() {
			if (window.pageYOffset > tabTopSpace) {
				tabHeadings.classList.add("sticky");
			} else {
				tabHeadings.classList.remove("sticky");
			}
		};
	}

	/**
	 * Displays a tab section based on ID
	 * Moves Tab line to the corresponding tab heading
	 * Hides all other tab sections
	 * @Input tabSectionID - ID of tab section to display
	 */
	showTab(tabSectionID) {
		this.selectedTab = tabSectionID;
		for (let section of this.tabSections) {
			section.style.display = "none";
		}
		let tabHeading = document.getElementById(`${tabSectionID}-heading`);
		document.getElementById(tabSectionID).style.display = "block";
		this.tabLine.setAttribute(
			"style",
			`
			transform: translate3d(${tabHeading.offsetLeft}px, 0px, 0px);
			-webkit-transform: translate3d(${tabHeading.offsetLeft}px, 0px, 0px);
			-moz-transform: translate3d(${tabHeading.offsetLeft}px, 0px, 0px);
			-o-transform: translate3d(${tabHeading.offsetLeft}px, 0px, 0px);
			width: ${tabHeading.offsetWidth}px`
		);
	}
}
