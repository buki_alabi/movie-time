export class CastList {
	constructor() {
		this.castStencil = [
			{
				imageSrc: "assets/doodles/aginghipster.svg",
				background: "linear-gradient(to top, #fad0c4 0%, #ffd1ff 100%)",
				character: "Lead Character"
			},

			{
				imageSrc: "assets/doodles/artist.svg",
				background: "linear-gradient(120deg, #84fab0 0%, #8fd3f4 100%)",
				character: "Co-lead Character"
			},

			{
				imageSrc: "assets/doodles/bearded_man_with_glasses.svg",
				background: "linear-gradient(to top, #cfd9df 0%, #e2ebf0 100%)",
				character: "The Best Friend"
			},

			{
				imageSrc: "assets/doodles/long_hair_gal.svg",
				background: "linear-gradient(to top, #fddb92 0%, #d1fdff 100%)",
				character: "The Investigative Reporter"
			}
		];
	}

	getCastCard(index, actor) {
		return `
		<section class="card hover-card">
			<div class="img-wrapper" style="background-image: ${
				this.castStencil[index]["background"]
			}">
				<img src=${this.castStencil[index]["imageSrc"]} />
			</div>
			<div class="center-text">
				<h3>${actor.trim()}</h3>
				<p class="light-text">${this.castStencil[index]["character"]}</p>
			</div>
		</section>
		`;
	}

	getRenderedCastList(castString) {
		let castList = castString.split(",");
		let renderedCastList = "";

		for (let [index, actor] of castList.entries()) {
			renderedCastList += this.getCastCard(index % 4, actor);
		}
		return renderedCastList;
	}
}
