export class Ratings {
	constructor() {}

	getRenderedRatings(movie) {
		let ratingTemplate = "";

		const ratings = movie.Ratings;
		for (let [index, rating] of ratings.entries()) {
			if (index < 3) {
				ratingTemplate += `<div class="rating">
					<h3>${rating.Source}</h3>
					<p>${rating.Value}</p>
				</div>`;
			} else {
				break;
			}
		}

		return (
			ratingTemplate +
			`<div> <span class="label" id="rated">${movie["Rated"] || "--"}</span></div>`
		);
	}
}
