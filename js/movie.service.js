import { ErrorService } from "./error.service.js";

export class MovieService {
	constructor() {
		//would be in some environment variable to distinguish dev/staging/production endpoints
		this.apiUrl = "http://www.omdbapi.com";
		//would normally be loaded from local storage after some authentication process
		this.apiKey = "cb8ab996";
		this.errorService = new ErrorService();
	}

	getMovie(title) {
		title = title.replace(" ", "+");
		let endpoint = `${this.apiUrl}/?apikey=${this.apiKey}&t=${title}`;
		return fetch(endpoint)
			.then(res => {
				if (!res.ok) throw Error(res.statusText);
				return res.json();
			})
			.catch(e => {
				this.errorService.showError("Error fetching movie", e);
			});
	}
}
