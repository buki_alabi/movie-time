export class ErrorService {
	constructor() {}

	showError(title, message) {
		console.error(`${title} |`, message);
		document.getElementById("error-heading").innerHTML = title;
		document.getElementById("error-message").innerHTML = message;
		document.getElementById("error").style.left = "20px";
		setTimeout(() => {
			document.getElementById("error").style.left = "-350px";
			setTimeout(() => {
				document.getElementById("error-heading").innerHTML = "";
				document.getElementById("error-message").innerHTML = "";
			}, 400);
		}, 3500);
	}
}
