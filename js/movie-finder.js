import { CastList } from "./cast-list.js";
import { ErrorService } from "./error.service.js";
import { ModalService } from "./modal.service.js";
import { MovieService } from "./movie.service.js";
import { Ratings } from "./ratings.js";
import { TabService } from "./tab.service.js";

export class MovieFinder {
	constructor() {
		this.movieService = new MovieService();
		this.modalService = new ModalService();
		this.tabService = new TabService();
		this.errorService = new ErrorService();
		this.castList = new CastList();
		this.ratings = new Ratings();
		this.loader = document.getElementById("loading");
		this.movie = null;
		this.firstTime = true;
		this.main = document.getElementsByTagName("main")[0];
		this.footer = document.getElementsByTagName("footer")[0];
		this.searchInput = document.getElementById("search-input");
	}

	initialize() {
		this.modalService.initializeAllModals();
		this.tabService.initializeTabs();
		this.initializeMoreMenu();
		this.handleMovieSearch();
		this.setMainHeight();
		window.addEventListener("resize", () => {
			this.setMainHeight();
		});
	}

	handleMovieSearch() {
		document.forms["search-form"].addEventListener("submit", event => {
			event.preventDefault();
			this.loadMovie(this.searchInput.value);
			this.searchInput.value = null;
		});
	}

	async loadMovie(searchText) {
		if (searchText.trim() === "") {
			this.errorService.showError(
				"No search text entered",
				"Please type in a valid movie title"
			);
			return;
		}
		this.loader.style.display = "block";
		this.movie = await this.movieService.getMovie(searchText);
		if (this.movie) {
			if ("Error" in this.movie || this.movie.Response.toLowerCase() !== "true") {
				this.errorService.showError(
					`Error loading ${searchText}`,
					this.movie["Error"]
				);
			} else {
				if (this.firstTime) {
					this.unwelcome();
					this.firstTime = false;
				}
				this.renderElements(this.movie);
				document.getElementById("overview-heading").click();
				this.modalService.closeModal("search");
			}
		}
		this.loader.style.display = "none";
	}

	unwelcome() {
		document.getElementById("welcome").style.display = "none";
		document.getElementsByTagName("nav")[0].style.visibility = "visible";
		document.getElementById("sub-header").style.visibility = "visible";
		document.getElementById("overview").style.visibility = "visible";
		document.getElementById("cast").style.visibility = "visible";
	}

	initializeMoreMenu() {
		let menu = document.getElementById("more_menu");
		document.getElementById("more_menu_toggle").addEventListener("click", () => {
			menu.style.display = menu.style.display === "block" ? "none" : "block";
		});

		window.onclick = function(event) {
			if (event.target.id !== "more_menu_toggle") {
				document.getElementById("more_menu").style.display = "none";
			}
		};
	}

	setMainHeight() {
		this.main.style.minHeight = `calc(100vh - ${this.main.offsetTop +
			this.footer.offsetHeight}px)`;
	}

	renderElements(movie) {
		document.getElementById("poster").src = movie["Poster"];
		document.getElementById("movie-title").innerHTML = movie["Title"] || "--";
		document.getElementById("genre").innerHTML = movie["Genre"] || "--";
		document.getElementById("year").innerHTML = movie["Year"] || "--";
		document.getElementById("runtime").innerHTML = movie["Runtime"] || "--";
		document.getElementById("plot").innerHTML = movie["Plot"] || "--";
		document.getElementById("released").innerHTML = movie["Released"] || "--";
		document.getElementById("director").innerHTML = movie["Director"] || "--";
		document.getElementById("writer").innerHTML = movie["Writer"] || "--";
		document.getElementById("production").innerHTML = movie["Production"] || "--";
		document.getElementById("website").innerHTML = movie["Website"] || "--";
		document.getElementById("website").href =
			movie["Website"] || "http://www.imdb.com";
		document.getElementById(
			"cast-list"
		).innerHTML = this.castList.getRenderedCastList(movie["Actors"]);
		this.modalService.initializeCastCards();
		document.getElementById(
			"ratings"
		).innerHTML = this.ratings.getRenderedRatings(movie);
	}
}
