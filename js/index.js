import { MovieFinder } from "./movie-finder.js";

(() => {
	const movieFinder = new MovieFinder();
	movieFinder.initialize();
	document.getElementById("search-btn").click();
})();
