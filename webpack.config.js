const path = require("path");

module.exports = {
	entry: {
		app: ["babel-polyfill", "./js/index.js"]
	},
	output: {
		path: path.resolve(__dirname, "dist"),
		filename: "index.bundle.js"
	},
	module: {
		rules: [
			{
				test: /\.js?$/,
				exclude: /node_modules/,
				loader: "babel-loader",
				query: {
					presets: ["env", "stage-0"]
				}
			}
		]
	},
	mode: "development"
};
