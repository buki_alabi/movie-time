import { MovieService } from "../js/movie.service.js";

let movieService = new MovieService();

describe("Sanity Test", () => {
	describe("true", () => {
		it("should return true no matter what", () => {
			assert.equal(true, true);
		});
	});
});

describe("Movie Service", () => {
	let passMovie, failMovie;

	before(async () => {
		passMovie = await movieService.getMovie("The Devil Wears Prada");
		failMovie = await movieService.getMovie("~");
	});

	it("should create an instance of the movie service", () => {
		should.exist(movieService);
	});

	it("should successfully find a movie titled 'The Devil Wears Prada'", () => {
		assert.equal(passMovie.Response, "True");
	});

	it("should fail if I search for an incoherent symbol like '~'", () => {
		assert.equal(failMovie.Response, "False");
	});
});
