import { MovieFinder } from "../js/movie-finder.js";

let movieFinder = new MovieFinder();
movieFinder.initialize();
let result;

describe("Movie Finder (on initialization)", () => {
	it("should create an instance of the movie finder", () => {
		should.exist(movieFinder);
	});

	it("should show a welcome message on initialization", () => {
		result = getComputedStyle(document.getElementById("welcome")).display;
		result.should.equal("block");
	});

	it("should hide nav on initialization", () => {
		result = getComputedStyle(document.getElementsByTagName("nav")[0]).visibility;
		result.should.equal("hidden");
	});

	it("should hide sub header on initialization", () => {
		result = getComputedStyle(document.getElementById("sub-header")).visibility;
		result.should.equal("hidden");
	});

	it("should hide overview tab on initialization", () => {
		result = getComputedStyle(document.getElementById("overview")).visibility;
		result.should.equal("hidden");
	});

	it("should hide cast tab on initialization", () => {
		result = getComputedStyle(document.getElementById("cast")).visibility;
		result.should.equal("hidden");
	});

	it("should not show test tab on initialization", () => {
		result = getComputedStyle(document.getElementById("tester")).display;
		result.should.equal("none");
	});

	it("should not show more_menu on initialization", () => {
		result = getComputedStyle(document.getElementById("more_menu")).display;
		result.should.equal("none");
	});
});

describe("Movie Finder (After a bad search)", () => {
	before(async () => {
		await movieFinder.loadMovie("~");
	});

	it("should move the error messenger into view after 400 milliseconds", () => {
		setTimeout(() => {
			result = getComputedStyle(document.getElementById("error")).left;
			assert.equal(result, "20px");
		}, 500);
	});

	it("should move the error messenger out of view after 3500 milliseconds", () => {
		setTimeout(() => {
			result = getComputedStyle(document.getElementById("error")).left;
			assert.equal(result, "-350px");
		}, 4000);
	});
});

describe("Movie Finder (After a good search)", () => {
	before(async () => {
		await movieFinder.loadMovie("Harry Potter");
	});

	it("should successfully load a movie object from the API", () => {
		console.log("Loaded movie: ", movieFinder.movie); //TODO delete
		assert.equal(movieFinder.movie.Response, "True");
	});

	it("should hide the welcome message after the first movie is successfully loaded", () => {
		result = getComputedStyle(document.getElementById("welcome")).display;
		result.should.equal("none");
	});

	it("should show nav", () => {
		result = getComputedStyle(document.getElementsByTagName("nav")[0]).visibility;
		result.should.equal("visible");
	});

	it("should show sub header", () => {
		result = getComputedStyle(document.getElementById("sub-header")).visibility;
		result.should.equal("visible");
	});

	it("should show overview", () => {
		result = getComputedStyle(document.getElementById("overview")).visibility;
		result.should.equal("visible");
	});

	it("should show cast tab", () => {
		result = getComputedStyle(document.getElementById("cast")).visibility;
		result.should.equal("visible");
	});

	it("should display the movie poster & other details (see UI below)", () => {
		let result = document.getElementById("poster").src;
		result.should.equal(movieFinder.movie.Poster);
	});

	it("should list all the cast members", () => {
		let result = document.querySelectorAll("#cast-list section.card");
		assert.lengthOf(result, movieFinder.movie.Actors.split(",").length);
	});

	it("should display the rating of the movie", () => {
		let result = document.getElementById("rated").innerHTML;
		assert.equal(result, movieFinder.movie.Rated);
	});

	it("should show the more_menu when the menu icon is clicked", () => {
		document.getElementById("more_menu_toggle").click();
		result = getComputedStyle(document.getElementById("more_menu")).display;
		assert.equal(result, "block");
	});

	it("should hide the more_menu if any other part of the page is clicked", () => {
		document.getElementById("main-header").click();
		result = getComputedStyle(document.getElementById("more_menu")).display;
		assert.equal(result, "none");
	});
});
