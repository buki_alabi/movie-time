import { TabService } from "../js/tab.service.js";

let tabService = new TabService();
let result;

describe("Tab Service", () => {
	it("should create an instance of the tab service", () => {
		should.exist(tabService);
	});

	it("should display appropriate tab by ID", () => {
		tabService.showTab("tester");
		result = getComputedStyle(document.getElementById("tester")).display;
		assert.equal(result, "block");
	});

	it("should move tab line under the selected tab's corresponding heading", () => {
		result = document.getElementById(`${tabService.selectedTab}-heading`)
			.offsetLeft;
		let tabLineTransform = document.getElementById("tab-line").style.transform;
		assert.equal(tabLineTransform, `translate3d(${result}px, 0px, 0px)`);
	});

	it("length of tab line should match tab heading", () => {
		result = document.getElementById(`${tabService.selectedTab}-heading`)
			.offsetWidth;
		let tabLineLength = document.getElementById("tab-line").style.width;
		assert.equal(tabLineLength, `${result}px`);
	});

	after(() => {
		tabService.showTab("overview");
	});
});
